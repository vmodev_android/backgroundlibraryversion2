package com.vmodev.modules.backgroundservice.actions;

public abstract class Action<Result> {
	private Result result;
	private Exception mException;

	public Action() {
	}

	protected abstract Result doInBackground() throws Exception;

	public final Exception execute() {
		try {
			result = doInBackground();
		} catch (Exception e) {
			mException = e;
		}
		onCompleted(result, mException);
		return mException;
	}

	protected void onCompleted(Result result, Exception exception) {

	}

	public final Result getResult() {
		return result;
	}

}
