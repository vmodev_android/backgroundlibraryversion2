package com.vmodev.modules.backgroundservice.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public abstract class BaseHttpAction<Result> extends Action<Result> {
	public static final int POST = 0;
	public static final int GET = 1;
	private int timeoutConnection = 60000;
	private int timeoutSocket = 60000;

	private HttpMethod method = HttpMethod.GET;
	private String url;
	private List<NameValuePair> nameValueBody;
	private List<NameValuePair> nameValueHeader;
	private String rawBody = null;

	public BaseHttpAction(String url, HttpMethod method) {
		this.url = url;
		if (!this.url.contains("?")) {
			this.url += "?";
		}
		this.method = method;
	}

	@Override
	protected Result doInBackground() throws Exception {
		onPrepareData();
		String response = getStringResponse();
		Result res = null;
		try {
			res = parseResultObject(response);
		} catch (Exception e) {
			throw new Exception("Can't parse json to object (" + e.getMessage()
					+ ")");
		}
		if (res == null) {
			throw new Exception("Result object is null");
		}
		return res;
	}

	protected void onPrepareData() throws Exception {

	}

	protected abstract Result parseResultObject(String response)
			throws Exception;

	private String getStringResponse() throws UnsupportedEncodingException,
			IOException, ClientProtocolException {
		HttpParams params = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(params, timeoutConnection);
		HttpConnectionParams.setSoTimeout(params, timeoutSocket);
		DefaultHttpClient httpClient = new DefaultHttpClient(params);
		HttpUriRequest request = getUriRequest();
		// add parameters to header
		if (nameValueHeader != null) {
			for (NameValuePair param : nameValueHeader) {
				request.addHeader(param.getName(), param.getValue());
			}
		}
		// get response from server
		HttpResponse response = httpClient.execute(request);
		// parse response to text
		BufferedReader br = null;
		String output = null;
		StringBuilder builder = new StringBuilder();
		br = new BufferedReader(new InputStreamReader(
				(response.getEntity().getContent())));
		while ((output = br.readLine()) != null) {
			builder.append(output);
		}
		httpClient.getConnectionManager().shutdown();
		return builder.toString();
	}

	private HttpUriRequest getUriRequest() throws UnsupportedEncodingException {
		switch (method) {
		case GET:
			return new HttpGet(url);
		case POST:
			HttpPost httpPost = new HttpPost(url);
			if (rawBody != null) {
				httpPost.setEntity(new ByteArrayEntity(rawBody
						.getBytes("UTF-8")));
			} else {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValueBody,
						"UTF-8"));
			}
			return httpPost;
		}
		return null;
	}

	protected void addParamToUrl(String key, String value) {
		if (!url.endsWith("?")) {
			url += "&";
		}
		url += key + "=" + value;
	}

	protected void addBasicParamToBody(String name, String value) {
		if (nameValueBody == null) {
			nameValueBody = new ArrayList<NameValuePair>();
		}
		nameValueBody.add(new BasicNameValuePair(name, value));
	}

	protected void addParamToHeader(String name, String value) {
		if (nameValueHeader == null) {
			nameValueHeader = new ArrayList<NameValuePair>();
		}
		nameValueHeader.add(new BasicNameValuePair(name, value));
	}

	public void setRawBody(String rawBody) {
		this.rawBody = rawBody;
	}
}