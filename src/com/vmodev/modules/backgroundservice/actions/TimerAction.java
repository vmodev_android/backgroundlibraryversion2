package com.vmodev.modules.backgroundservice.actions;

import android.content.Context;

public abstract class TimerAction<Result> extends Action<Result> {
	private Context context;

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}
}
