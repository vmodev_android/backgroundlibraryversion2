package com.vmodev.modules.backgroundservice;

import com.vmodev.modules.backgroundservice.actions.Action;

public enum ClientCommon {
	INSTANT;
	public void executeNonCallBack(Action<?> action) {
		int id = Util.generateRequestId();
		ExecutorManager.getInstant().post(new Request(id, action, false));
	}
}
