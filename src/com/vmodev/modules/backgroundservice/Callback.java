package com.vmodev.modules.backgroundservice;

import com.vmodev.modules.backgroundservice.actions.Action;

public interface Callback<R, A extends Action<?>> {
	public void onFailureRequest(A action, Exception exception);

	/**
	 * result object is always not null here
	 */
	public void onSuccessRequest(A action, R result);
}
