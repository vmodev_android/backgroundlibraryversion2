package com.vmodev.modules.backgroundservice;

import com.vmodev.modules.backgroundservice.actions.Action;

/**
 * Use to send to executor
 * 
 */
class Request {
	public int id;
	public Action<?> action;
	public boolean needCallbackUI;

	public Request(int id, Action<?> action, boolean needCallback) {
		this.id = id;
		this.action = action;
		this.needCallbackUI = needCallback;
	}
}
