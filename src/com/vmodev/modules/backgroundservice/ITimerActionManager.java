package com.vmodev.modules.backgroundservice;

import java.util.Date;

import android.content.Context;

import com.vmodev.modules.backgroundservice.actions.TimerAction;

public interface ITimerActionManager {

	public void create(Context context);

	public void destroy();

	public void scheduleAction(String keyTimer, TimerAction<?> TimerAction,
			long delay);

	public void scheduleAction(String keyTimer, TimerAction<?> TimerAction,
			Date when);

	public void scheduleAction(String keyTimer, TimerAction<?> TimerAction,
			long delay, long duration);

	public void scheduleAction(String keyTimer, TimerAction<?> TimerAction,
			Date when, long delay);

	public void cancelAction(String keyTimer);

}
