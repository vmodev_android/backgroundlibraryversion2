package com.vmodev.modules.backgroundservice;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.vmodev.modules.backgroundservice.actions.Action;

import de.greenrobot.event.EventBus;

/**
 * Manager the event bus and a subscriber which will execute all of actions in
 * separate thread and notify to update UI
 */
class ExecutorManager {
	private static ExecutorManager instant = null;

	/*
	 * Input action event buses: this bus will handle all of actions which are
	 * posted from UI client
	 */
	private EventBus actionInputBus = null;

	private ExecutorManager() {
		actionInputBus = new EventBus();
		actionInputBus.register(new MySubscriberAsync());
	}

	public static ExecutorManager getInstant() {
		if (instant == null) {
			instant = new ExecutorManager();
		}
		return instant;
	}

	public void post(Request request) {
		actionInputBus.post(request);
	}

	public static class MySubscriberAsync {
		public void onEventAsync(Request request) {
			Exception ex = null;
			Action<?> action = request.action;
			ex = action.execute();
			if (request.needCallbackUI) {
				// get context
				Context context = Config.getContext();
				if (context != null) {
					if (ex == null) {
						sendSuccessResponse(context, request);
					} else {
						sendFailureResponse(context, request, ex);
					}
				} else {
					VLog.e("Can not send response without context. Please setup for it");
				}
			}
		}

		public void sendSuccessResponse(Context context, Request request) {
			String actionId = Util.getBroadcastName(request.id);
			Intent intent = new Intent(actionId);
			intent.putExtra(ClientManager.PARAM_STATUS,
					ClientManager.RESULT_SUCESS);
			LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
		}

		public void sendFailureResponse(Context context, Request request,
				Exception ex) {
			String actionId = Util.getBroadcastName(request.id);
			Intent intent = new Intent(actionId);
			intent.putExtra(ClientManager.PARAM_STATUS,
					ClientManager.RESULT_FAILURE);
			intent.putExtra(ClientManager.PARAM_EXCEPTION, ex);
			LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
		}
	}

}
