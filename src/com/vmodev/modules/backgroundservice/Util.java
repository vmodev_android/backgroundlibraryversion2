package com.vmodev.modules.backgroundservice;


class Util {
	private static String BROADCAST_ACTION_NAME = "client:manager:";
	private static volatile int idCounter = 0;

	public static String getBroadcastName(int id) {
		return BROADCAST_ACTION_NAME + id;
	}

	public synchronized static int generateRequestId() {
		idCounter++;
		return idCounter;
	}
}
