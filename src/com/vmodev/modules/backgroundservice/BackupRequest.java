package com.vmodev.modules.backgroundservice;

import android.content.BroadcastReceiver;

import com.vmodev.modules.backgroundservice.actions.Action;

public class BackupRequest<A> {
	public int id;
	public Callback<?, ? extends Action<?>> callback;
	public Action<?> action;
	public BroadcastReceiver receiver;

	public BackupRequest(int id, Callback<?, ? extends Action<?>> callback,
			Action<?> action, BroadcastReceiver receiver) {
		this.id = id;
		this.callback = callback;
		this.action = action;
		this.receiver = receiver;
	}
}
