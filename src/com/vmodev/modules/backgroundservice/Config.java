package com.vmodev.modules.backgroundservice;

import java.lang.ref.WeakReference;

import android.content.Context;

/**
 * Must setup for it before use background module.
 * 
 */
public class Config {
	private static WeakReference<Context> contextReference;

	/**
	 * Should pass the context of application or service to parameter. Don't
	 * pass the context of activity
	 * 
	 * @param context
	 */
	public static void setup(Context context) {
		contextReference = new WeakReference<Context>(context);
	}

	public static void enableLog(boolean enable) {
		VLog.debug = enable;
	}

	public static Context getContext() {
		Context context = null;
		if (contextReference != null) {
			context = contextReference.get();
		}
		return context;
	}
}
