package com.vmodev.modules.backgroundservice;

import java.lang.ref.WeakReference;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.SparseArray;

import com.vmodev.modules.backgroundservice.actions.Action;

/**
 * Should use a separate instant of ClientManager for every
 * activities/fragments. ClientManager send action to executor, receive
 * callback... Also release all of reference after UI was destroyed
 * 
 */
public class ClientManager {
	public static String TAG = "Client manager";
	public static final int RESULT_SUCESS = 1;
	public static final int RESULT_FAILURE = 0;
	public static final String PARAM_STATUS = "STATUS";
	public static final String PARAM_ACTION_ID = "ACTION_ID";
	public static final String PARAM_EXCEPTION = "EXCEPTION";

	private WeakReference<Context> contextReference = null;
	private SparseArray<BackupRequest> requestStore;

	public void start(Context context) {
		contextReference = new WeakReference<Context>(context);
		requestStore = new SparseArray<BackupRequest>();
	}

	public void release() {
		int size = requestStore.size();
		for (int index = 0; index < size; index++) {
			BackupRequest request = requestStore.valueAt(index);
			unregisterGetResponse(request);
		}
		contextReference.clear();
		requestStore.clear();
	}

	public void execute(Action<?> action,
			Callback<?, ? extends Action<?>> callback) {
		int id = Util.generateRequestId();
		boolean needCallback = callback != null;
		if (needCallback) {
			BackupRequest request = new BackupRequest(id, callback, action,
					new CompletedActionReceiver(id));
			requestStore.append(id, request);
			registerGetResponse(request);
		} else {
			// don't need store request
		}
		VLog.v("Send action id =" + id + " via event bus");
		ExecutorManager.getInstant()
				.post(new Request(id, action, needCallback));
	}

	private void registerGetResponse(BackupRequest request) {
		Context context = contextReference.get();
		if (context != null) {
			IntentFilter intentFilter = new IntentFilter(
					Util.getBroadcastName(request.id));
			LocalBroadcastManager.getInstance(context).registerReceiver(
					request.receiver, intentFilter);
		} else {
			VLog.e("Can not get context");
		}
	}

	private void unregisterGetResponse(BackupRequest request) {
		Context context = contextReference.get();
		if (context != null && request.receiver != null) {
			LocalBroadcastManager.getInstance(context).unregisterReceiver(
					request.receiver);
		} else {
			VLog.e("[Unregister] Can not get context");
		}
	}

	private class CompletedActionReceiver extends BroadcastReceiver {
		private int actionId;

		public CompletedActionReceiver(int actionId) {
			this.actionId = actionId;
		}

		@Override
		public void onReceive(Context context, Intent intent) {
			int result = intent.getIntExtra(PARAM_STATUS, RESULT_FAILURE);
			BackupRequest request = requestStore.get(actionId);
			if (request == null) {
				VLog.e("[Library error] Can not found the request with id ="
						+ actionId);
				return;
			}
			if (result == RESULT_SUCESS) {
				VLog.v("Handle Success reponse from action " + actionId);
				request.callback.onSuccessRequest(request.action,
						request.action.getResult());
			} else if (result == RESULT_FAILURE) {
				VLog.v("Handle Failure reponse from action " + actionId);
				Exception exception = (Exception) intent
						.getSerializableExtra(PARAM_EXCEPTION);
				request.callback.onFailureRequest(request.action, exception);
			}
			unregisterGetResponse(request);
		}
	}
}
